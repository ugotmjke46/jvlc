package org.videolan.jvlc.swt;

import java.util.Dictionary;
import java.util.Hashtable;

import org.eclipse.swt.SWT;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;

public class VideoWidget extends Composite {

	private long jvlcHandle;
	
	static Dictionary<Long, VideoWidget> table = new Hashtable<Long, VideoWidget>();
	
	public VideoWidget(Composite parent, int style)
	{
		super(parent, style);
		long handleParent = handle;
		jvlcHandle = createControl(handleParent);
		if (jvlcHandle == 0) SWT.error (SWT.ERROR_NO_HANDLES);
		table.put (jvlcHandle, this);
		
		addDisposeListener (new DisposeListener () {
            public void widgetDisposed (DisposeEvent e) {
            	VideoWidget.this.widgetDisposed (e);
            }

        });
	}

	protected void widgetDisposed(DisposeEvent e)
	{
		table.remove(jvlcHandle);
		jvlcHandle = 0;
	}

    static void widgetSelected (long handle) {
    	VideoWidget videoWidget = (VideoWidget) table.get (new Long(handle));
        if (videoWidget == null) return;
        videoWidget.notifyListeners (SWT.Selection, new Event ());
    }
	
	private native long createControl(long handleParent);

}
